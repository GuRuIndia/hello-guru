<?php
include_once dirname ( __FILE__ ) . '/includes/connect.php';
$users = UserTable::retrieveAll ();
?>
<a href="user_add.php">Add User</a> <br /><br />
<table width="100%" cellspacing="05" cellpadding="05" border="1">
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Email</th>
		<th>Birth Date</th>
		<th>Actions</th>
	</tr>
	<?php if(count($users) > 0): ?>
	 <?php foreach($users as $user):?>
	 <tr>
		<td><?php echo $user->getId()?></td>
		<td><?php echo $user->getFirstName()." ".$user->getLastName()?></td>
		<td><?php echo $user->getEmail()?></td>
		<td><?php echo $user->getBirthDate()?></td>
		<td>
		  <a href="user_edit.php?id=<?php echo $user->getId() ?>">Edit</a> |
		  <a href="user_delete.php?id=<?php echo $user->getId() ?>" onclick="return confirm('Are you sure ?')">Delete</a> 
		</td>
	</tr>
	 <?php endforeach; ?>
	<?php endif; ?>
</table>