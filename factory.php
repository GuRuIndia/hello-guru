<?php

/**
 * CD Factory
 *
 * @author ritesh
 */
class CDFactory {

  /**
   * Create()
   * This function will create an object given class name.
   *
   * @param string $type          
   * @return object new object
   */
  public static function create($type) {
    $class = strtolower ( $type ) . 'CD';
    return new $class ();
  }
}

class standardCD {
  private $title = "";
  private $band = "";
  private $tracks = array ();

  public function __construct() {
  }

  public function setTitle($title) {
    $this->title = $title;
  }

  public function setBand($band) {
    $this->band = $band;
  }

  public function addTrack($track) {
    $this->tracks [] = $track;
  }

  public function display() {
    echo "<b>Class Name:</b>&nbsp;" . __CLASS__ . "<br />";
    echo "<b>Title:</b>&nbsp;{$this->title}<br />";
    echo "<b>Band:</b>&nbsp;{$this->band}<br />";
    echo "<b>Tracks:</b>&nbsp;" . json_encode ( $this->tracks );
    echo "<hr />";
  }
}

class enhancedCD {
  private $title = "";
  private $band = "";
  private $tracks = array ();

  public function __construct() {
    $this->tracks [] = "DATA TRACK";
  }

  public function setTitle($title) {
    $this->title = $title;
  }

  public function setBand($band) {
    $this->band = $band;
  }

  public function addTrack($track) {
    $this->tracks [] = $track;
  }

  public function display() {
    echo "<b>Class Name:</b>&nbsp;" . __CLASS__ . "<br />";
    echo "<b>Title:</b>&nbsp;{$this->title}<br />";
    echo "<b>Band:</b>&nbsp;{$this->band}<br />";
    echo "<b>Tracks:</b>&nbsp;" . json_encode ( $this->tracks );
    echo "<hr />";
  }
}

echo "<h1>Factory Pattern Example</h1>";

$type = "enhanced";
$cd = CDFactory::create ( $type );
$cd->setBand ( "Elephunk" );
$cd->setTitle ( "Fusion" );
$tracks = array (
    'James Bay - LET IT GO',
    'Jems Smith Disclouser',
    'Rita Ora Fill the love' 
);
foreach ( $tracks as $track ) {
  $cd->addTrack ( $track );
}
$cd->display ();

$type = "standard";
$cd = CDFactory::create ( $type );
$cd->setBand ( "Elephunk" );
$cd->setTitle ( "Fusion" );
$tracks = array (
    'Sean Paul, Enrique - Bailando',
    'Rihana - Whats my name',
    'Aksent - Stay with me' 
);
foreach ( $tracks as $track ) {
  $cd->addTrack ( $track );
}
$cd->display ();
?>
