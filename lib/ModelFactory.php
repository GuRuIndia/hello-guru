 <?php

class ModelFactory {

  public static function createInstance($model) {
    return new $model();
  }
  
}