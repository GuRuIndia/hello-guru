<?php
class UserTable{
  public static function retrieveAll(){
    global $dbopt;
    $query = "SELECT * FROM user";
    return $dbopt->fetch_results('User', $query);
  }
  
  public static function retrieveById($id){
    global $dbopt;
    $query = "SELECT * FROM user WHERE id = $id";
    $records = $dbopt->fetch_results('User', $query);
    return count($records) > 0 ? $records[0] : false;
  }
}