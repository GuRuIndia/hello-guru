<?php

class User {
  private $id;
  private $first_name;
  private $last_name;
  private $email;
  private $birth_date;

  public function __construct() {
   
  }
  
  public function getTableName(){
    return 'user';
  }
  
  public function getProperties(){
    return get_object_vars($this);
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getFirstName() {
    return $this->first_name;
  }

  public function setFirstName($first_name) {
    $this->first_name = $first_name;
  }

  public function getLastName() {
    return $this->last_name;
  }

  public function setLastName($last_name) {
    $this->last_name = $last_name;
  }

  public function getEmail() {
    return $this->email;
  }

  public function setEmail($email) {
    $this->email = $email;
  }

  public function getBirthDate($format = 'Y-m-d') {
    return $this->birth_date > 0 ? date ( $format, strtotime ( $this->birth_date ) ) : '';
  }

  public function setBirthDate($birth_date) {
    $this->birth_date = $birth_date;
  }
  
  public function save(){
  	global $dbopt;
  	if($this->id){
  	  $dbopt->update($this, 'id', $this->id);
  	} else {
  	  $newId = $dbopt->insert($this);
  	  $this->setId($newId);
  	} 
  }
  
  public function delete(){
    global $dbopt;
    $dbopt->delete($this, 'id', $this->id);
  }
}
?>
