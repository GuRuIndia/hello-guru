<?php
include_once dirname ( __FILE__ ) . '/includes/connect.php';

$id = $_GET ['id'];
$user = UserTable::retrieveById ( $id );

if ($user) {
  $user->delete ();
}

redirect('user_list.php');
?>