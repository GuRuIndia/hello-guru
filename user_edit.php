<?php
include_once dirname ( __FILE__ ) . '/includes/connect.php';

$id = $_GET['id'];
$user = UserTable::retrieveById($id);

if($user){
  $first_name = $user->getFirstName();
  $last_name = $user->getLastName();
  $email = $user->getEmail();
  $birth_date = $user->getBirthDate();
}

if(isset($_POST['submit'])){
  $first_name = $_POST['first_name'];
  $last_name = $_POST['last_name'];
  $email = $_POST['email'];
  $birth_date = $_POST['birth_date'];
  
  $errors = array();
  
  if(trim($first_name) == ""){
    $errors['first_name'] = 'First name is required';
  }
  
  if(trim($last_name) == ""){
    $errors['last_name'] = 'Last name is required';
  }
  
  if(trim($email) == ""){
    $errors['email'] = 'Email is required';
  }
  
  if(trim($birth_date) == ""){
    $errors['birth_date'] = 'Birth Date is required';
  }
  
  if(count($errors) == 0){
    $user->setFirstName($first_name);
    $user->setLastName($last_name);
    $user->setEmail($email);
    $user->setBirthDate($birth_date);
    $user->save();
    redirect('user_list.php');
  }
}
?>
<form id="frm_user_add" name="frm_user_add" method="POST" action="">
	<div>
		<label>First Name</label> <input type="text" id="first_name"
			name="first_name" value="<?php echo $first_name ?>" />
<?php if(isset($errors['first_name'])): ?>
<p class="error"><?php echo $errors['first_name']; ?></p>
<?php endif; ?>
</div>
	<div>
		<label>Last Name</label> <input type="text" id="last_name"
			name="last_name" value="<?php echo $last_name ?>" />
<?php if(isset($errors['last_name'])): ?>
<p class="error"><?php echo $errors['last_name']; ?></p>
<?php endif; ?>
</div>
	<div>
		<label>Email</label> <input type="text" id="email" name="email"
			value="<?php echo $email ?>" />
<?php if(isset($errors['email'])): ?>
<p class="error"><?php echo $errors['email']; ?></p>
<?php endif; ?>
</div>
	<div>
		<label>Birth Date</label> <input type="text" id="birth_date"
			name="birth_date" value="<?php echo $birth_date ?>" />
<?php if(isset($errors['birth_date'])): ?>
<p class="error"><?php echo $errors['birth_date']; ?></p>
<?php endif; ?>
</div>
	<div>
		<input type="submit" id="submit" name="submit" value="Save" /> <input
			type="button" id="cancel" name="cancel" value="Cancel"
			onclick="window.location = 'user_list.php';" />
	</div>
</form>
