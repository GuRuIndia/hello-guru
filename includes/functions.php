<?php

function redirect($url, $is_parent = false) {
  if ($is_parent) {
    echo '<script type="text/javascript">
          window.parent.location = "' . $url . '";
        </script>';
  } else {
    echo '<script type="text/javascript">
          window.location = "' . $url . '";
        </script>';
  }
  exit;
}

?>