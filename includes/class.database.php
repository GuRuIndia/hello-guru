<?php

class Database {
  private $_connection;
  private static $_instance;
  private $_host = 'localhost';
  private $_username = 'root';
  private $_password = 'root73';
  private $_database = 'core';
  
  /*
   * Get an instance of the Database @return Instance
   */
  public static function getInstance() {
    if (! self::$_instance) { // If no instance then make one
      self::$_instance = new self ();
    }
    return self::$_instance;
  }
  // Constructor
  private function __construct() {
    $this->_connection = new mysqli ( $this->_host, $this->_username, $this->_password, $this->_database );
    
    // Error handling
    if (mysqli_connect_error ()) {
      trigger_error ( "Failed to conencto to MySQL: " . mysql_connect_error (), E_USER_ERROR );
    }
  }
  // Magic method clone is empty to prevent duplication of connection
  private function __clone() {
  }
  // Get mysqli connection
  public function getConnection() {
    return $this->_connection;
  }
}

class dbOpt {
  public $mysqli;

  public function __construct() {
    $db = Database::getInstance ();
    $this->mysqli = $db->getConnection ();
  }

  public function fetch_results($model, $query) {
    $result = $this->mysqli->query ( $query );
    $res = array ();
    
    if ($result) {
      while ( $row = $result->fetch_assoc () ) {
        $modelObj = ModelFactory::createInstance ( $model );
        foreach ( $row as $column => $value ) {
          $method = "set" . str_replace ( ' ', '', ucwords ( str_replace ( '_', ' ', $column ) ) );
          $modelObj->$method ( $value );
        }
        $res [] = $modelObj;
      }
    }
    return $res;
  }

  public function insert($modelObj) {
    $properties = $modelObj->getProperties ();    
    $fields = $values = "";
    foreach ( $properties as $column => $value ) {
      if ($value) {
        $fields .= $column.", ";
        $values .= "'$value', ";
      }
    }
    
    $fields = trim($fields, ", ");
    $values = trim($values, ", ");
    
    $query = "INSERT INTO {$modelObj->getTableName()} ($fields)
              VALUES($values)";
    $this->mysqli->query ( $query );
    return $this->mysqli->insert_id;
  }

  public function update($modelObj, $compField, $compValue) {
    $properties = $modelObj->getProperties ();
    $updateData = "";
    foreach ( $properties as $column => $value ) {
      if ($value) {
        $updateData .= "$column = '$value', ";
      }
    }
  
    $updateData = trim($updateData, ", ");
    
    $query = "UPDATE {$modelObj->getTableName()} SET {$updateData} 
              WHERE {$compField} = {$compValue}";
        
    $this->mysqli->query ( $query );
  }
  
  public function delete($modelObj, $compField, $compValue){
    $query = "DELETE FROM {$modelObj->getTableName()} 
              WHERE {$compField} = {$compValue}";
    
    $this->mysqli->query ( $query );
  }
}
?>